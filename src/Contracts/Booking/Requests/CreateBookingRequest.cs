﻿namespace Contracts.Booking.Requests;

public record CreateBookingRequest(
    string OrderId,
    DateTime FromDate,
    DateTime ToDate,
    string Floor,
    int BedCount);

