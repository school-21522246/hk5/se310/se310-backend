﻿namespace Contracts.Booking;

public record BookingResponse(
    Guid Id,
    Guid UserId,
    string OrderId,
    DateTime FromDate,
    DateTime ToDate,
    string Floor,
    int BedCount);

