﻿namespace Contracts.Room.Requests;

public record CreateRoomRequest(
    string Name,
    string Description,
    string Floor,
    int BedCount,
    decimal Amount,
    string Currency,
    ICollection<string> Images);
