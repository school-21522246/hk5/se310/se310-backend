namespace Application.Rooms;

public record FloorResult(
    int Id,
    string Name)
{
    // public RoomResult()
    //     : this(Guid.Empty,
    //            string.Empty,
    //            string.Empty,
    //            false,
    //            string.Empty,
    //            0,
    //            0,
    //            string.Empty,
    //            new List<string>())
    // { }
}
