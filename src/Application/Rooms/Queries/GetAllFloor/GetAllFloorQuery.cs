﻿namespace Application.Rooms.Queries.GetAllFloor;

public record GetAllFloorQuery() : IQuery<ICollection<FloorResult>>;
