﻿namespace Application.Rooms.Queries.GetAllFloor;

internal sealed class GetAllFloorQueryHandler(
    IRoomRepository roomRepository,
    IMapper mapper) : IQueryHandler<GetAllFloorQuery, ICollection<FloorResult>>
{
    public async Task<Result<ICollection<FloorResult>>> Handle(GetAllFloorQuery request, CancellationToken cancellationToken)
    {
        var floors = await roomRepository.GetAllFloors(cancellationToken);
        return floors.Select(mapper.Map<FloorResult>).ToList();
    }
}
