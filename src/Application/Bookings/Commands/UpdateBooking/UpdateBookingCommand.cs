﻿namespace Application.Bookings.Commands.UpdateBooking;

public record UpdateBookingCommand(
    Guid BookingId,
    DateTime? FromDate,
    DateTime? ToDate,
    string? Floor,
    int? BedCount) : ICommand<BookingResult>;
