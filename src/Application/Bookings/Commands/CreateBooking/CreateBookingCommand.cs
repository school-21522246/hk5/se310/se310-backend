﻿namespace Application.Bookings.Commands.CreateBooking;

public record CreateBookingCommand(
    Guid UserId,
    string OrderId,
    DateTime FromDate,
    DateTime ToDate,
    string Floor,
    int BedCount) : ICommand<BookingResult>;
