﻿using Application.Users;

namespace Application.Bookings;

public record BookingResult(
    Guid Id,
    string OrderId,
    DateTime FromDate,
    DateTime ToDate,
    string Floor,
    int BedCount,
    UserResult User);
